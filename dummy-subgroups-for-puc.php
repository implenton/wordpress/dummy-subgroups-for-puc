<?php
/*
 * Plugin Name: Dummy Subgroups for PUC
 * Plugin URI:  https://gitlab.com/implenton/wordpress/dummy-subgroups-for-puc
 * Description: A dummy plugin for testing PUC update for GitLab subgroups
 * Version:     0.4
 * Author:      implenton
 * Author URI:  https://implenton.com
 * License:     GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
*/

define('DUMMY_SUBGROUP', '0.4');

require dirname(__FILE__) . '/plugin-update-checker/plugin-update-checker.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/implenton/wordpress/dummy-subgroups-for-puc',
    __FILE__,
    'dummy-subgroups-for-puc'
);

add_action('admin_notices', function () {
    ?>

    <div class="notice notice-success">
        <p>Dummy Subgroups for PUC: <strong><?php echo DUMMY_SUBGROUP; ?></strong></p>
    </div>

    <?php
});
